/**
 * Created by guest on 4/03/16.
 */
public class Box extends Shape {
    int box_size = 3;
    @Override
    void draw() {
        for(int i = 0; i < box_size; i++) {
            for (int j = 0; j < box_size; j ++) {
                System.out.print("#");
            }
            System.out.println();
        }
    }
}
